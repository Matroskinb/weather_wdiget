const request = require('request');
const weatherConfig = require('../../config/weather');

class ForecastClient{
  get() {
    return new Promise((resolve, reject) => {
      request.get({
        url: weatherConfig.baseUrl,
        qs: {
          appid: weatherConfig.key,
          id: weatherConfig.ids.Moscow,
        },
        headers: {
          'Content-Type': 'application/json',
        }
      }, (err, responce, body) => {
        if (err) {
          console.log(`error[REQUEST, API]: ${err}`);
          reject(err);
        } else {
          resolve(responce);
        }
      });
    });
  }
}

module.exports = ForecastClient;