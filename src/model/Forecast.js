const ForecastCache = require('./ForecastCache');
const ForecastClient = require('./ForecastClient');

class Forecast {
  constructor(city, days, client = false, cache = false) {
    if (!client)
      this.client = new ForecastClient();
    if (!cache)
      this.cache = new ForecastCache(city);
  }

  get(daysCount = 5) {
    return new Promise((resolve, reject) => {
      this.cache.get(daysCount)
        .then((result) => {
          if (result.length === 0) {
            this.client.get()
              .then((res) => {
                let jsonRes = JSON.parse(res.body);
                this.cache.cache(jsonRes);
                resolve(jsonRes.list.splice(0, daysCount));
              });
          } else {
            resolve(result);
          }
        })
        .catch((err) => reject(err));
    });
  }
}

module.exports = Forecast;