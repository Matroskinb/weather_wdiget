const weatherConfig = require('../../config/weather');
const redis = require('../db/redis');
const Days = require('./Days');

class ForecastCache {
    constructor(city) {
        this.city = city;
        this.key = `weather:${city}`;
        this.days = new Days();
    }

    get(daysCount) {
        return new Promise((resolve, reject) => {
            redis.zrangebyscore([this.key, '(' + this.days.today,
                this.days.getTimeByOffset(daysCount)], (err, days) => {
                if (err)
                    reject(err);
                else 
                    resolve(days.map((el) => JSON.parse(el)));
            });
        });

    }

    cache({ list }) {
        list.forEach((el) => redis.zadd(this.key, el.dt, JSON.stringify(el)));
    }

}

module.exports = ForecastCache;