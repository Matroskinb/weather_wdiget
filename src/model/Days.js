class Days{
  constructor() {
    this.date = new Date();
    this.currentHour = this.date.getHours();
    this.currentDay = this.date.getDate();
    this.date.setHours(0);
    this.date.setMinutes(0);
    this.today = this.getUnixTimestamp();
  }

  getUnixTimestamp() {
    return this.date.getTime() / 1000 | 0;
  }

  setDay(daysOffset) {
    this.date.setDate(daysOffset);
    return this;
  }

  getByOffset(daysOffset) {
    return this.setDay(this.currentDay + Number.parseInt(daysOffset));
  }

  getTimeByOffset(daysOffset) {
    return this.getByOffset(daysOffset).getUnixTimestamp();
  }
}

module.exports = Days;