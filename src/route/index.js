const pug = require('pug');
const express = require('express');
const router = express.Router();

const weatherConfig = require('../../config/weather');
const Forecast = require('../model/Forecast');

router.get('/forecast_widget', (req, res) => {
  const widget = new Forecast(req.query);
  res.render('layout/widget', { data });
});
router.get('/about', (req, res) => res.render('page/about'));
router.get('/', (req, res) =>
  res.render('page/home', { cities: weatherConfig.cities, days: weatherConfig.daysCount }));

module.exports = router;
