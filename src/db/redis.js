const redis = require('redis');
const dbConfig = require('../../config/db');

const client = redis.createClient(dbConfig);

client.on('error', (err) => console.log(`Error[REDIS]: ${err}`));

module.exports = client;