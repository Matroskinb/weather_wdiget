export default class WidgetForm {
  constructor({ optionInputs, resultInput, widgetPlace }) {
    this.inputs = Array.from(optionInputs);
    this.resultInput = resultInput;
    this.widgetPlace = widgetPlace;
    
    this.handleChange = this.handleChange.bind(this);

    optionInputs.forEach((el) => {
      el.addEventListener('change', this.handleChange);
      el.addEventListener('input', this.handleChange);
    });
  }

  handleChange(e) {
    return this.render();
  }

  inputToQuery(el) {
    return el.name + '=' + el.value;
  }

  filterInputs(el) {
    if (el.type === 'radio' || el.type === 'checkbox')
      return el.checked
    else
      return true;
  }

  getOptions() {
    return this.inputs.filter((el) => this.filterInputs(el))
      .map((el) => el.name + '=' + el.value).join('&');
  }

  render() {
    const iframe = `<iframe frameborder="0" src="${document.location.origin}/forecast_widget?${this.getOptions()}"></iframe>`;
    this.resultInput.innerHTML = iframe;
    this.widgetPlace !== undefined && this.widgetPlace !== null ? this.widgetPlace.innerHTML = iframe : '';
  }
}