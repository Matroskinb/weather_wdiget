import WidgetForm from './WidgetForm';

window.addEventListener('load', () => {
  const widgetForm = new WidgetForm({
    optionInputs: document.querySelectorAll('#widget-settings input, #widget-settings select'),
    resultInput: document.querySelector('#widget-settings textarea'),
    widgetPlace: document.querySelector('#widget-place'),
  });
});