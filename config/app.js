const appConfig = {
  paths: {
    static: process.env.STATIC_FILES_PATH || 'public',
    views: 'views',
  },
  views_engine: 'pug',
};

module.exports = appConfig;