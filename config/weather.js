const weatherConfig = {
    key: 'cfa8f8fcd4f4b06647c48da4a88d7e06',
    baseUrl: 'http://api.openweathermap.org/data/2.5/forecast',
    iconUrl: (iconName) => `http://openweathermap.org/img/w/${iconName}.png`,
    cities: {
        Moscow: 524901,
        SaintPetersburg: 519690,
        NizhnyNovgorod: 519336,
    },
    daysCount: [1,3,5]
}

module.exports = weatherConfig;